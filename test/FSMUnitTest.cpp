//
// Created by Andres Duque on 7/7/2021.
//

#include "gtest/gtest.h"
#include "Sun.h"
 class FSMUnitTest : public ::testing::Test{
  protected:
  Sun sun;
  public:
  FSMUnitTest() = default;
  ~FSMUnitTest() override = default;
};

TEST_F(FSMUnitTest, FSMOkay){
  sun.Live();
  sun.Record();
  sun.Result();
}

TEST_F(FSMUnitTest, FSMIllegalOkay){
  sun.Result();
  sun.Live();
  sun.Record();
}