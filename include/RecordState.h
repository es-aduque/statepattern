//
// Created by Andres Duque on 7/7/2021.
//

#pragma once

#include "SunState.h"

class RecordState : public SunState{
 public:
  RecordState() : SunState("Record Mode"){}
  ~RecordState() override = default;
  void result(Sun * sun) override;
};