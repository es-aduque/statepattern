//
// Created by Andres Duque on 7/7/2021.
//

#pragma once

#include <memory>

#include "SunState.h"

class SunState; // Forward declaration

class Sun{
 private:
  SunState * m_state = nullptr;
 public:
  enum class State{
    LIVE,
    RECORD,
    RESULT
  };
  Sun();
  ~Sun(){
    delete m_state;
  }
  void Live();
  void Record();
  void Result();
  void setState(State state);

};