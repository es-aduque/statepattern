//
// Created by Andres Duque on 7/7/2021.
//

#pragma once

#include <string>
#include <memory>

class Sun;

class SunState{
 public:
  explicit SunState(std::string name);
  virtual ~SunState();
  virtual void live(Sun * sun);
  virtual void record(Sun * sun);
  virtual void result(Sun * sun);
  std::string getName();
 private:
  std::string   m_name;
};