//
// Created by Andres Duque on 7/7/2021.
//

#pragma once

#include "SunState.h"

class LiveState : public SunState{
 public:
  LiveState() : SunState("Live Mode"){}
  ~LiveState() override = default;
  void record(Sun * sun) override;
};