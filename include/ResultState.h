//
// Created by Andres Duque on 7/7/2021.
//

#pragma once

#include "SunState.h"
class ResultState : public SunState{
 public:
  ResultState();
  ~ResultState() override = default;
  void live(Sun * sun) override;
};
