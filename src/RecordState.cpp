//
// Created by Andres Duque on 7/7/2021.
//

#include "Sun.h"
#include "RecordState.h"

void RecordState::result(Sun * sun) {
  sun->setState(Sun::State::RESULT);
}
