#include "SunState.h"

#include <utility>
#include <iostream>

SunState::SunState(std::string name) {
  m_name = std::move(name);
}
void SunState::live(Sun * sun) {
  std::cout << "Illegal state transition from " << getName() << " to Live" << std::endl;
}
void SunState::record(Sun * sun) {
  std::cout << "Illegal state transition from " << getName() << " to Record" << std::endl;

}
void SunState::result(Sun * sun) {
  std::cout << "Illegal state transition from " << getName() << " to Result" << std::endl;
}
std::string SunState::getName() {
  return m_name;
}
SunState::~SunState() = default;
