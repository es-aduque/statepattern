//
// Created by Andres Duque on 7/7/2021.
//

#include "Sun.h"
#include "ResultState.h"

ResultState::ResultState() : SunState("Result Mode"){}

void ResultState::live(Sun * sun) {
  sun->setState(Sun::State::LIVE);
}
