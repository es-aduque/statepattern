//
// Created by Andres Duque on 7/7/2021.
//

#include <iostream>

#include "Sun.h"
#include "LiveState.h"
#include "RecordState.h"
#include "ResultState.h"

Sun::Sun() {
  m_state = new ResultState();
}

void Sun::Live() {
  m_state->live(this);
}
void Sun::Record() {
  m_state->record(this);
}
void Sun::Result() {
  m_state->result(this);
}
void Sun::setState(Sun::State state) {
  std::cout << "changing from " << m_state->getName() << " to ";
  if(state == State::LIVE){
    m_state = new LiveState();
  }else if (state == State::RECORD){
    m_state = new RecordState();
  }else{
    m_state = new ResultState();
  }
  std::cout << m_state->getName() << " state" << std::endl;
}
