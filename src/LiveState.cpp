//
// Created by Andres Duque on 7/7/2021.
//

#include "LiveState.h"
#include "Sun.h"

void LiveState::record(Sun * sun) {
  sun->setState(Sun::State::RECORD);
}

